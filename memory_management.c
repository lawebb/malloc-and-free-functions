#include "memory_management.h"
#include <sys/types.h>
#include <unistd.h>


void *head = NULL;    //Pointer to start of list
struct memblock *block;   //Initialising blocks
int splitFail;    //Variable to tackle failed freeblock splits

void * _malloc(size_t size){
	if(size == 0) {
		return NULL;		//Return NULL if size is smaller than or equal to 0
	}

  if(size % 8 != 0) {
    size += 8 - (size % 8);   //Aligning size for any variable
  }

	if(!head) {		//If there are currently are no blocks
		block = sbrk(0);		//Set block to current top of memory

		sbrk(size + bh_size);   //Allocate space for size + header.

		block->size = size;		//Set the block size
		block->free = 0;		//Set free to false
    block->next = NULL;   //Set the pointer to next block to NULL
    //to show it is the last block

		head = block;   //Assign the head pointer to the first element
	}
	else {   //If there already exists blocks
    struct memblock *prevblock = head;
    struct memblock *curblock = head;

		while(curblock && !(curblock->free && curblock->size >= size)) {
      prevblock = curblock;
			curblock = curblock->next;   //Increment prevblock and curblock to search
      //blocks for a free block of valid size
		}

    block = curblock;

		if(block) {		//Free block found
			if(block->size == size) {
				block->free = 0;    //If fits perfectly, set free to false
			}
			else if(block->size > (size + bh_size)) {    //If the free block is too
      //large, split the block in 2 parts, one to allocate to the new malloc request
      //and one to remain free in linked list
        struct memblock *new = head;
				new->size = block->size - (size + bh_size);
				new->free = 1;
				new->next = block->next;

				block->size = size;
				block->free = 0;
				block->next = new;
			}
			else {   //If an appropriate free block wasn't found, set splitFail to 1
				splitFail = 1;
			}
		}

		if(!block || splitFail) {		//No free block
			splitFail = 0;

      block = sbrk(0);    //Set block pointer to current top of memory and then
      //request more memory and assign the new memory
      sbrk(size + bh_size);

      prevblock->next = block;

			block->size = size;
			block->free = 0;
			block->next = NULL;
		}
	}
	return block;		//Return a pointer to the allocated memory
}

void _free(void * ptr){
  struct memblock *freeblock = head;
  struct memblock *prevblock = head;
  struct memblock *nextblock = head;

  int count = 0;    //Set while counter to 0

  if(!ptr) {
    return;   //If NULL pointer passed, return
  }

  while((freeblock != ptr) && (freeblock)){   //while freeblock isn't the
  //passed argument and freeblock isn't NULL, increment prevblock, freeblock
  //and count
    prevblock = freeblock;
    freeblock = freeblock->next;
    count += 1;
  }
  if(freeblock == ptr && freeblock->free != 1) {    //If pointer match found,
    //and it hasn't already been called, free is set to true
    freeblock->free = 1;

    if(freeblock->next) {   //If next block after freeblock isn't NULL
      nextblock = freeblock->next;
      if(nextblock->free == 1) {    //If nextblock is free, merge nextblock
        //into freeblock
        freeblock->size = freeblock->size + nextblock->size + bh_size;
        freeblock->next = nextblock->next;
        nextblock = NULL;
      }
    }
    if(prevblock->free == 1 && count >= 1) {    //If prevblock is free, and
      //freeblock isn't the head of the list, merge freeblock into prevblock
      prevblock->size = prevblock->size + freeblock->size + bh_size;
      prevblock->next = freeblock->next;
      freeblock = NULL;
      if(!prevblock->next) {    //If prevblock is the last block in the list,
        //decrement the program memory and prevblock size by 4000 until the
        //final block is smaller than 4k
        while(prevblock->size > 4000){
          prevblock->size -= 4000;
          sbrk(-4000);
        }
      }
    }
    else if(!freeblock->next) {     //If freeblock is the last block in the list,
      //decrement the program memory and freeblock size by 4000 until the
      //final block is smaller than 4k
      while(freeblock->size > 4000) {
        freeblock->size -= 4000;
        sbrk(-4000);
      }
    }
  }
}
