# Malloc and free functions

My implementation of the malloc and free functions in C.

# Why I made this programm

I made this programm as part of a project in university second year. It was
very interesting to see how functions like malloc actually work and allocate the
space required for C programs.


# Features

    * Malloc function returns a void pointer to the start of allocated memory.
        This memory is allocated on the heap and the pointer is aligned depending
        on the variable type.
    * Free function frees memory and makes it available for subsequent
        allocations. It will return memory to the operating system when there
        is appropriate amounts free.
    
    
# How to run on Linux

Put the c code in the same directory as another c source file. Include the
header file into that new file, and then call the functions on your own code.
(Check where the pointer now points to see if it worked).


# Specification

[Link 1: Specification](https://gitlab.com/lawebb/malloc-and-free-functions/-/blob/master/specification/comp2211_cw1%20(1).pdf)