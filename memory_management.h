#include <sys/types.h>

#define bh_size sizeof(memblock)    //Defining the block header size

typedef struct memblock memblock;

struct memblock {
  size_t size;      //Size of memory block
  int free;     //Is block free? 1 = yes, 0 = no
  struct memblock *next;    //Pointer to next block
};

void * _malloc(size_t size);

void _free(void * ptr);
